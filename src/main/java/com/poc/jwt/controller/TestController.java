package com.poc.jwt.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping({ "/" })
public class TestController {

	@GetMapping(value = "/test")
	public ResponseEntity<?> hello() {
		return ResponseEntity.status(HttpStatus.OK).body("This is for jwt testing !");
	}

}
