package com.poc.jwt.model;

import java.io.Serializable;

public class AuthenticateResponse implements Serializable {

	private static final long serialVersionUID = 690919680015167993L;
	private String jwttoken;
	private String username;
	private String role;

	public AuthenticateResponse() {
		super();
	}


	public AuthenticateResponse(String jwttoken, String username, String role) {
		super();
		this.jwttoken = jwttoken;
		this.username = username;
		this.role = role;
	}


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getJwttoken() {
		return jwttoken;
	}

	public void setJwttoken(String jwttoken) {
		this.jwttoken = jwttoken;
	}

	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	@Override
	public String toString() {
		return "AuthenticateResponse [jwttoken=" + jwttoken + ", username=" + username + "]";
	}

}